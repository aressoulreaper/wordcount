package co.za.codeproblem.wordcount.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler
{
  @ExceptionHandler(value = {ApiRequestException.class})
  public ResponseEntity<Object> handleApiRequestException(ApiRequestException exception)
  {
    HttpStatus status = HttpStatus.BAD_REQUEST;
    ApiException apiException = new ApiException(exception.getMessage(), status, ZonedDateTime.now(ZoneId.systemDefault()));
    return new ResponseEntity<>(apiException, status);
  }
}
