package co.za.codeproblem.wordcount.exception;

public class ApiRequestException extends RuntimeException
{
  public ApiRequestException(String message)
  {
    super(message);
  }
}