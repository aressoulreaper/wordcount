package co.za.codeproblem.wordcount.impl;

import co.za.codeproblem.wordcount.interfaces.WordFrequency;

import java.util.Map;

public class Frequency implements WordFrequency
{
  private final Map<String, Integer> freqMap;

  public Frequency(Map<String, Integer> freqMap)
  {
    this.freqMap = freqMap;
  }
  public int getFrequencyForProvidedWord(String searchKey)
  {
    return freqMap.get(searchKey);
  }
  @Override
  public String getWord()
  {
    return freqMap.entrySet().stream()
        .max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1)
        .get()
        .getKey();
  }
  @Override
  public int getFrequency()
  {
    return freqMap.entrySet().stream()
        .max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1)
        .get()
        .getValue();
  }
}