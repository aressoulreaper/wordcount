package co.za.codeproblem.wordcount.impl;

import co.za.codeproblem.wordcount.interfaces.WordFrequency;
import co.za.codeproblem.wordcount.interfaces.WordFrequencyAnalyzer;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Service("wordFrequencyAnalyzer")
public class WordFrequencyAnalyzerImpl implements WordFrequencyAnalyzer
{
  private static final String REGX_PATTERN = "[\\s.]";

  @Override
  public int calculateHighestFrequency(String text)
  {
    return new Frequency(generateFrequencyMapFromProvidedText(text)).getFrequency();
  }

  @Override
  public int calculateFrequencyForWord(String text, String word)
  {
    return new Frequency(generateFrequencyMapFromProvidedText(text)).getFrequencyForProvidedWord(word);
  }

  @Override
  public List<WordFrequency> calculateMostFrequentNWords(String text, int n)
  {
    List<String> wordsList = retrieveTheMostFrequentWordsToProvidedLimit(text.toLowerCase(), n);
    Map<String, Integer> freqMap = generateFrequencyMapFromProvidedText(text.toLowerCase());
    return wordsList.stream().map(str -> new WordFrequencyImpl(str, freqMap.get(str))).collect(Collectors.toList());
  }

  private Map<String, Integer> generateFrequencyMapFromProvidedText(String text)
  {
    Map<String, Integer> freqMap = new HashMap<>();
    asList(text.trim().split(REGX_PATTERN)).forEach(s -> freqMap.compute(s, (s1, count) -> count == null ? 1 : count + 1));
    return freqMap;
  }

  private List<String> retrieveTheMostFrequentWordsToProvidedLimit(String text, int limit)
  {
    return asList(text.trim().split(REGX_PATTERN)).stream().collect(Collectors.collectingAndThen(
        Collectors.groupingBy(Function.identity(), Collectors.counting()),
        map -> map.entrySet().stream()
            .sorted(Comparator.<Map.Entry<String, Long>>comparingLong(Map.Entry::getValue).reversed()
                .thenComparing(Map.Entry::getKey))
            .map(Map.Entry::getKey)
            .limit(limit)
            .collect(Collectors.toList())));
  }
}
