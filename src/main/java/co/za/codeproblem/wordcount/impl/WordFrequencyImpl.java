package co.za.codeproblem.wordcount.impl;

import co.za.codeproblem.wordcount.interfaces.WordFrequency;

public class WordFrequencyImpl implements WordFrequency
{
  private final String word;
  private final int frequency;

  public WordFrequencyImpl(String word, int frequency)
  {
    this.word = word;
    this.frequency = frequency;
  }
  @Override
  public String getWord()
  {
    return this.word;
  }
  @Override
  public int getFrequency()
  {
    return this.frequency;
  }
}