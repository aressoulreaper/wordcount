package co.za.codeproblem.wordcount.commands;

import co.za.codeproblem.wordcount.exception.ApiRequestException;
import co.za.codeproblem.wordcount.webservices.request.FrequencyRequestBody;
import co.za.codeproblem.wordcount.webservices.response.FrequencyResponse;

public class CalculateFrequencyForProvidedWordCmd extends CalculateFrequencyCmd
{
  public CalculateFrequencyForProvidedWordCmd(FrequencyRequestBody frequencyRequestBody)
  {
    super(frequencyRequestBody);
  }

  @Override
  public void execute()
  {
    if (doValidations())
    {
      determineHighestFrequencyForWord();
    }
    else
    {
      throw new ApiRequestException("One or more of the required fields are null");
    }
  }

  private void determineHighestFrequencyForWord()
  {
    frequencyResponse = new FrequencyResponse();
    frequencyResponse.setFrequency(wordFrequencyAnalyzer.calculateFrequencyForWord(frequencyRequestBody.getText(), frequencyRequestBody.getWord()));
  }

  @Override
  protected boolean doValidations()
  {
    return frequencyRequestBody != null
        && frequencyRequestBody.getText() != null
        && frequencyRequestBody.getWord() != null
        && !frequencyRequestBody.getText().trim().isEmpty()
        && !frequencyRequestBody.getWord().trim().isEmpty();
  }
}
