package co.za.codeproblem.wordcount.commands;

import co.za.codeproblem.wordcount.exception.ApiRequestException;
import co.za.codeproblem.wordcount.webservices.request.FrequencyRequestBody;
import co.za.codeproblem.wordcount.webservices.response.FrequencyResponse;

public class CalculateHighestFrequencyCmd extends CalculateFrequencyCmd
{
  public CalculateHighestFrequencyCmd(FrequencyRequestBody frequencyRequestBody)
  {
    super(frequencyRequestBody);
  }

  @Override
  public void execute()
  {
    if (doValidations())
    {
      determineHighestFrequency();
    }
    else
    {
      throw new ApiRequestException("One or more required fields are either null or empty");
    }
  }

  private void determineHighestFrequency()
  {
    frequencyResponse = new FrequencyResponse();
    frequencyResponse.setFrequency(wordFrequencyAnalyzer.calculateHighestFrequency(frequencyRequestBody.getText()));
  }

  public FrequencyResponse getFrequencyResponse()
  {
    return this.frequencyResponse;
  }
}
