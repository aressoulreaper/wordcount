package co.za.codeproblem.wordcount.commands;

import co.za.codeproblem.wordcount.impl.WordFrequencyAnalyzerImpl;
import co.za.codeproblem.wordcount.interfaces.ICommand;
import co.za.codeproblem.wordcount.webservices.request.FrequencyRequestBody;
import co.za.codeproblem.wordcount.webservices.response.FrequencyResponse;

public abstract class CalculateFrequencyCmd implements ICommand
{
  protected WordFrequencyAnalyzerImpl wordFrequencyAnalyzer = new WordFrequencyAnalyzerImpl();
  protected final FrequencyRequestBody frequencyRequestBody;
  protected FrequencyResponse frequencyResponse;

  public CalculateFrequencyCmd(FrequencyRequestBody frequencyRequestBody)
  {
    this.frequencyRequestBody = frequencyRequestBody;
  }

  @Override
  public void execute()
  {

  }

  protected boolean doValidations()
  {
    return frequencyRequestBody != null
        && frequencyRequestBody.getText() != null
        && !frequencyRequestBody.getText().trim().isEmpty();
  }

  public FrequencyResponse getFrequencyResponse()
  {
    return this.frequencyResponse;
  }
}
