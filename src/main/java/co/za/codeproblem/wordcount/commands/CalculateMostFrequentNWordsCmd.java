package co.za.codeproblem.wordcount.commands;

import co.za.codeproblem.wordcount.exception.ApiRequestException;
import co.za.codeproblem.wordcount.impl.WordFrequencyAnalyzerImpl;
import co.za.codeproblem.wordcount.interfaces.ICommand;
import co.za.codeproblem.wordcount.webservices.request.FrequencyRequestBody;
import co.za.codeproblem.wordcount.webservices.response.FrequencyLimitResponse;
import co.za.codeproblem.wordcount.webservices.response.FrequencyResponse;

import java.util.stream.Collectors;

public class CalculateMostFrequentNWordsCmd implements ICommand
{
  private final WordFrequencyAnalyzerImpl wordFrequencyAnalyzer = new WordFrequencyAnalyzerImpl();
  private final FrequencyRequestBody frequencyRequestBody;
  private FrequencyLimitResponse frequencyLimitResponse;

  public CalculateMostFrequentNWordsCmd(FrequencyRequestBody frequencyRequestBody)
  {
    this.frequencyRequestBody = frequencyRequestBody;
  }

  @Override
  public void execute()
  {
    if (doValidations())
    {
      determineHighestFrequencyForNWord();
    }
    else
    {
      throw new ApiRequestException("One or more of the required fields are null");
    }
  }
  private void determineHighestFrequencyForNWord()
  {
    frequencyLimitResponse = new FrequencyLimitResponse();
    frequencyLimitResponse.setWordFrequencies(wordFrequencyAnalyzer.calculateMostFrequentNWords(frequencyRequestBody.getText(), frequencyRequestBody.getLimit())
        .stream().map(wordFrequency -> new FrequencyResponse(wordFrequency.getWord(), wordFrequency.getFrequency()))
        .collect(Collectors.toList()));
  }

  private boolean doValidations()
  {
    return frequencyRequestBody != null
        && frequencyRequestBody.getText() != null
        && !frequencyRequestBody.getText().trim().isEmpty()
        && frequencyRequestBody.getLimit() > 0;
  }

  public FrequencyLimitResponse getFrequencyLimitResponse()
  {
    return frequencyLimitResponse;
  }
}
