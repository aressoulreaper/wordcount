package co.za.codeproblem.wordcount.commands;

import co.za.codeproblem.wordcount.interfaces.ICommand;

public class CommandDelegate
{
  private static CommandDelegate commandDelegate;

  /**
   * Private instance ensuring that there can only be one
   * */
  private CommandDelegate()
  {

  }

  /**
   * Returns the singleton instance if it exists, if it does not it will call the
   * createInstance method.
   *
   * @return CommandDelegate
   */
  public static CommandDelegate getInstance()
  {
    if(commandDelegate == null)
    {
      createInstance();
    }
    return commandDelegate;
  }

  /**
   * Singleton createInstance method improves performance
   * as <code>getInstance ()</code> is not synchronized
   */
  private static synchronized void createInstance()
  {
    if(commandDelegate == null)
    {
      commandDelegate = new CommandDelegate();
    }
  }

  /**
   * Gets an instance of the command, executes the command and returns the result
   *
   * @param command - the command to execute
   * @return ICommand
   */
  public static <T extends ICommand> T execute(T command)
  {
    return getInstance().executeCommand(command);
  }

  /**
   * Executes a command and returns the result. It is important to note that when the command is run in different JVM's
   * that the Command will be serialized (cloned) and thus the original reference should be reassigned to the result of
   * call to this method.
   */
  private  <T extends ICommand> T executeCommand(T command)
  {
    command.execute();
    return command;
  }
}