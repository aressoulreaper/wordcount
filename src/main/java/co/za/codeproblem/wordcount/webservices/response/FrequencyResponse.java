package co.za.codeproblem.wordcount.webservices.response;

import co.za.codeproblem.wordcount.impl.Frequency;
import co.za.codeproblem.wordcount.interfaces.WordFrequency;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FrequencyResponse implements Serializable, WordFrequency
{
  public FrequencyResponse()
  {

  }

  public FrequencyResponse(String word, int frequency)
  {
    this.word = word;
    this.frequency = frequency;
  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String word;
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private int frequency;

  public void setWord(String word)
  {
    this.word = word;
  }
  public void setFrequency(int frequency)
  {
    this.frequency = frequency;
  }
  @Override
  public String getWord()
  {
    return this.word;
  }
  @Override
  public int getFrequency()
  {
    return this.frequency;
  }
}