package co.za.codeproblem.wordcount.webservices.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class FrequencyLimitResponse
{
  @JsonProperty("wordFrequencies")
  private List<FrequencyResponse> wordFrequencies = new ArrayList<>();

  public List<FrequencyResponse> getWordFrequencies()
  {
    return wordFrequencies;
  }
  public void setWordFrequencies(List<FrequencyResponse> wordFrequencies)
  {
    this.wordFrequencies = wordFrequencies;
  }
}
