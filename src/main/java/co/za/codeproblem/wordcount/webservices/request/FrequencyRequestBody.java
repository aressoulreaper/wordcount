package co.za.codeproblem.wordcount.webservices.request;

import java.io.Serializable;
public class FrequencyRequestBody implements Serializable
{
  private String text;
  private String word;
  private int limit;

  public String getText()
  {
    return text;
  }

  public void setText(String text)
  {
    this.text = text;
  }

  public String getWord()
  {
    return word;
  }

  public void setWord(String word)
  {
    this.word = word;
  }

  public int getLimit()
  {
    return limit;
  }
  public void setLimit(int limit)
  {
    this.limit = limit;
  }
}