package co.za.codeproblem.wordcount.webservices.controller;

import co.za.codeproblem.wordcount.commands.CalculateFrequencyForProvidedWordCmd;
import co.za.codeproblem.wordcount.commands.CalculateHighestFrequencyCmd;
import co.za.codeproblem.wordcount.commands.CalculateMostFrequentNWordsCmd;
import co.za.codeproblem.wordcount.commands.CommandDelegate;
import co.za.codeproblem.wordcount.webservices.request.FrequencyRequestBody;
import co.za.codeproblem.wordcount.webservices.response.FrequencyLimitResponse;
import co.za.codeproblem.wordcount.webservices.response.FrequencyResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/ws/word-frequency")
public class WordFrequencyController
{
  @RequestMapping(
      value = "/calculate-highest-frequency",
      method = RequestMethod.POST,
      consumes = "application/json",
      produces = "application/json")
  public ResponseEntity<FrequencyResponse> calculateHighestFrequency(@RequestBody FrequencyRequestBody frequencyRequestBody)
  {
    CalculateHighestFrequencyCmd cmd = new CalculateHighestFrequencyCmd(frequencyRequestBody);
    cmd = CommandDelegate.execute(cmd);
    return new ResponseEntity<>(cmd.getFrequencyResponse(), new HttpHeaders(), HttpStatus.OK);
  }

  @RequestMapping(
      value = "/calculate-frequency-for-word",
      method = RequestMethod.POST,
      consumes = "application/json",
      produces = "application/json")
  public ResponseEntity<FrequencyResponse> calculateFrequencyForGivenWord(@RequestBody FrequencyRequestBody frequencyRequestBody)
  {
    CalculateFrequencyForProvidedWordCmd cmd = new CalculateFrequencyForProvidedWordCmd(frequencyRequestBody);
    cmd = CommandDelegate.execute(cmd);
    return new ResponseEntity<>(cmd.getFrequencyResponse(), new HttpHeaders(), HttpStatus.OK);
  }

  @RequestMapping(
      value = "/calculate-frequent-words-for-limit",
      method = RequestMethod.POST,
      consumes = "application/json",
      produces = "application/json")
  public ResponseEntity<FrequencyLimitResponse> calculateMostFrequentNWord(@RequestBody FrequencyRequestBody frequencyRequestBody)
  {
    CalculateMostFrequentNWordsCmd cmd = new CalculateMostFrequentNWordsCmd(frequencyRequestBody);
    cmd = CommandDelegate.execute(cmd);
    return new ResponseEntity<>(cmd.getFrequencyLimitResponse(), new HttpHeaders(), HttpStatus.OK);
  }
}