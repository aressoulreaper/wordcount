package co.za.codeproblem.wordcount.interfaces;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface WordFrequency
{
  @JsonProperty("word")
  String getWord();

  @JsonProperty("frequency")
  int getFrequency();
}