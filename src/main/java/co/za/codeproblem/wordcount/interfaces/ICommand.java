package co.za.codeproblem.wordcount.interfaces;

public interface ICommand
{
  void execute();
}