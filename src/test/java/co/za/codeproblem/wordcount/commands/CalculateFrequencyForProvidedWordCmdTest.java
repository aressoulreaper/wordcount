package co.za.codeproblem.wordcount.commands;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CalculateFrequencyForProvidedWordCmdTest extends CalculateFrequencyTest
{
  @Test
  @DisplayName("Expect that the frequency returned is 2")
  public void textCalculateFrequencyForProvidedWord()
  {
    frequencyRequestBody.setWord("on");
    CalculateFrequencyForProvidedWordCmd cmd = new CalculateFrequencyForProvidedWordCmd(frequencyRequestBody);
    cmd = CommandDelegate.execute(cmd);
    Assertions.assertEquals(2, cmd.getFrequencyResponse().getFrequency());
  }
}