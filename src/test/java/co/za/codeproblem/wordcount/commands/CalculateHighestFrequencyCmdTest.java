package co.za.codeproblem.wordcount.commands;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CalculateHighestFrequencyCmdTest extends CalculateFrequencyTest
{
  @Test
  @DisplayName("Expect that frequency returns 4")
  public void testCalculateHighestFrequencyCmd()
  {
    CalculateHighestFrequencyCmd cmd = new CalculateHighestFrequencyCmd(frequencyRequestBody);
    cmd = CommandDelegate.execute(cmd);
    Assertions.assertEquals(4, cmd.getFrequencyResponse().getFrequency());
  }
}