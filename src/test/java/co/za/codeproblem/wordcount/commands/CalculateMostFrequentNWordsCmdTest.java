package co.za.codeproblem.wordcount.commands;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CalculateMostFrequentNWordsCmdTest extends CalculateFrequencyTest
{
  @Test
  @DisplayName("List size should be 3 and should match string object value")
  public void testMostFrequentNWords() throws JsonProcessingException
  {
    final String EXPECTED = "{\"wordFrequencies\":[{\"word\":\"the\",\"frequency\":2},{\"word\":\"lake\",\"frequency\":1},{\"word\":\"over\",\"frequency\":1}]}";
    updateFrequencyRequestBody();
    CalculateMostFrequentNWordsCmd cmd = new CalculateMostFrequentNWordsCmd(frequencyRequestBody);
    cmd = CommandDelegate.execute(cmd);
    Assertions.assertEquals(3, cmd.getFrequencyLimitResponse().getWordFrequencies().size());
    ObjectMapper mapper = new ObjectMapper();
    Assertions.assertEquals(EXPECTED, mapper.writeValueAsString(cmd.getFrequencyLimitResponse()));
  }

  private void updateFrequencyRequestBody()
  {
    frequencyRequestBody.setText("The sun shines over the lake");
    frequencyRequestBody.setLimit(3);
  }
}