package co.za.codeproblem.wordcount.commands;

import co.za.codeproblem.wordcount.webservices.request.FrequencyRequestBody;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public abstract class CalculateFrequencyTest
{
  protected static FrequencyRequestBody frequencyRequestBody;

  @BeforeAll
  static void createTestData()
  {
    generateFrequencyRequestBody();
  }

  protected static void generateFrequencyRequestBody()
  {
    frequencyRequestBody = new FrequencyRequestBody();
    frequencyRequestBody.setText("Ann while Bob had had had had a better effect on on the teacher");
  }
}