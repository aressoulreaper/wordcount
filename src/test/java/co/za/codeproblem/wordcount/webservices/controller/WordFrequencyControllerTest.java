package co.za.codeproblem.wordcount.webservices.controller;
import co.za.codeproblem.wordcount.webservices.request.FrequencyRequestBody;
import co.za.codeproblem.wordcount.webservices.response.FrequencyLimitResponse;
import co.za.codeproblem.wordcount.webservices.response.FrequencyResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WordFrequencyControllerTest
{
  @LocalServerPort
  private int port;

  private static final String BASE_URL = "http://localhost:";

  @Autowired
  private TestRestTemplate restTemplate;

  protected static FrequencyRequestBody frequencyRequestBody;

  @Test
  @DisplayName("Test CalculateHighestFrequency web service with correct message body")
  public void testCalculateHighestFrequencyWithCorrectBody()
  {
    generateFrequencyRequestBody();
    ResponseEntity<FrequencyResponse> response = this.restTemplate
        .postForEntity(BASE_URL + port + "/ws/word-frequency/calculate-highest-frequency", frequencyRequestBody, FrequencyResponse.class);
    assertEquals(200, response.getStatusCodeValue());
    assertEquals(4, response.getBody().getFrequency());
  }

  @Test
  @DisplayName("Test CalculateHighestFrequency web service with incorrect message body")
  public void testCalculateHighestFrequencyWithIncorrectBody()
  {
    generateIncorrectFrequencyRequestBody();
    ResponseEntity<FrequencyResponse> response = this.restTemplate
        .postForEntity(BASE_URL + port + "/ws/word-frequency/calculate-highest-frequency", frequencyRequestBody, FrequencyResponse.class);
    assertEquals(400, response.getStatusCodeValue());
  }

  @Test
  @DisplayName("Test CalculateFrequencyForGivenWord web service with correct message body")
  public void testCalculateFrequencyForGivenWordWithCorrectBody()
  {
    generateFrequencyRequestBody();
    frequencyRequestBody.setWord("on");
    ResponseEntity<FrequencyResponse> response = this.restTemplate
        .postForEntity(BASE_URL + port + "/ws/word-frequency/calculate-frequency-for-word", frequencyRequestBody, FrequencyResponse.class);
    assertEquals(200, response.getStatusCodeValue());
    assertEquals(2, response.getBody().getFrequency());
  }

  @Test
  @DisplayName("Test CalculateFrequencyForGivenWord web service with incorrect message body")
  public void testCalculateFrequencyForGivenWordWithIncorrectCorrectBody()
  {
    generateFrequencyRequestBody();
    ResponseEntity<FrequencyResponse> response = this.restTemplate
        .postForEntity(BASE_URL + port + "/ws/word-frequency/calculate-frequency-for-word", frequencyRequestBody, FrequencyResponse.class);
    assertEquals(400, response.getStatusCodeValue());
  }

  @Test
  @DisplayName("Test CalculateMostFrequentNWord web service with correct message body")
  public void testCalculateMostFrequentNWordWithCorrectBody()
  {
    generateFrequencyRequestBody();
    frequencyRequestBody.setText("The sun shines over the lake");
    frequencyRequestBody.setLimit(3);
    ResponseEntity<FrequencyLimitResponse> response = this.restTemplate
        .postForEntity(BASE_URL + port + "/ws/word-frequency/calculate-frequent-words-for-limit", frequencyRequestBody, FrequencyLimitResponse.class);
    assertEquals(200, response.getStatusCodeValue());
    assertEquals(3, Objects.requireNonNull(response.getBody()).getWordFrequencies().size());
  }

  @Test
  @DisplayName("Test CalculateMostFrequentNWord web service with incorrect message body")
  public void testCalculateMostFrequentNWordWithIncorrectBody()
  {
    generateFrequencyRequestBody();
    frequencyRequestBody.setText("The sun shines over the lake");
    ResponseEntity<FrequencyLimitResponse> response = this.restTemplate
        .postForEntity(BASE_URL + port + "/ws/word-frequency/calculate-frequent-words-for-limit", frequencyRequestBody, FrequencyLimitResponse.class);
    assertEquals(400, response.getStatusCodeValue());
  }

  private static void generateFrequencyRequestBody()
  {
    frequencyRequestBody = new FrequencyRequestBody();
    frequencyRequestBody.setText("Ann while Bob had had had had a better effect on on the teacher");
  }

  private static void generateIncorrectFrequencyRequestBody()
  {
    frequencyRequestBody = new FrequencyRequestBody();
  }
}