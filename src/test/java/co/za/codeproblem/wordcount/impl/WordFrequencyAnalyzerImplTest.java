package co.za.codeproblem.wordcount.impl;

import co.za.codeproblem.wordcount.interfaces.WordFrequency;
import co.za.codeproblem.wordcount.interfaces.WordFrequencyAnalyzer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class WordFrequencyAnalyzerImplTest
{
  private static final String TEXT = "Ann while Bob had had had had a better effect on on the teacher";

  @Autowired
  private WordFrequencyAnalyzer wordFrequencyAnalyzer;

  @Test
  @DisplayName("Expect that Frequency returns 4")
  public void testCalculateHighestFrequency()
  {
    Assertions.assertEquals(4, wordFrequencyAnalyzer.calculateHighestFrequency(TEXT));
  }

  @Test
  @DisplayName("Expect that Frequency returns `on`")
  public void testCalculateFrequencyForWord()
  {
    Assertions.assertEquals(2, wordFrequencyAnalyzer.calculateFrequencyForWord(TEXT, "on"));
  }

  @Test
  @DisplayName("Expect list to return 3 values and object as strings to match")
  public void testCalculateMostFrequentNWords() throws JsonProcessingException
  {
    String egText = "The sun shines over the lake";
    final String EXPECTED = "[{\"word\":\"the\",\"frequency\":2},{\"word\":\"lake\",\"frequency\":1},{\"word\":\"over\",\"frequency\":1}]";
    List<WordFrequency> wordList = wordFrequencyAnalyzer.calculateMostFrequentNWords(egText, 3);
    Assertions.assertEquals(3, wordList.size());
    ObjectMapper mapper = new ObjectMapper();
    Assertions.assertEquals(EXPECTED, mapper.writeValueAsString(wordList));
  }
}