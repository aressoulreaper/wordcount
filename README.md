# WordCount

A standalone spring boot application to be part of a text processing library.

## Getting started

### Dependencies
To be able to run the application, you will be required to have the following installed on your local machine:
* Java 8 or later
* Apache Maven 3.8.5 or later
* Git

### Installation and Execution
* Clone the repository using git from the following link (https://gitlab.com/aressoulreaper/wordcount.git)
* Once cloned, open your terminal/command line to the base directory of the repository
* Run the following command
```
mvn clean install
```
* Once the process has been completed, through the terminal navigate to the /target directory
* Run the following command 
````
java -jar WordCount-1.0.0.jar
````
* Once completed, the application would have started up and is ready to use.

## How to use
The application has a built-in swagger frontend that will allow you to run any API calls from your browser.
To access the swagger frontend, navigate to the following link [Swagger Frontend](http://localhost:8080/swagger-ui.html).
Additionally, should you wish to access the API directly with and external tool, please see the links listed below.

* [POST] (http://localhost:8080/ws/word-frequency/calculate-highest-frequency) - Calculate Highest Frequency
````
{
    "text": "Ann while Bob had had had had a better effect on on the teacher"
}
````
* [POST] (http://localhost:8080/ws/word-frequency/calculate-frequency-for-word) - Calculate Frequency for word
````
{
    "text": "Ann while Bob had had had had a better effect on on the teacher",
    "word": "on"
}
````
* [POST] (http://localhost:8080/ws/word-frequency/calculate-frequent-words-for-limit) - Calculate N* Frequent words
````
{
    "text": "Ann while Bob had had had had a better effect on on the teacher",
    "limit": 3
}
````

## Testing
The application code was developed with testing in mind and therefor Unit Testing and Integration Testing has been applied.
The tests can be run directly from the command line/terminal. To execute the tests only, use the following command within
the base directory of the application.
````
mvn test
````

## Authors
* Walter Oberholzer